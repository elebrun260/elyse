board = [["-","-","-"],
         ["-","-","-"],
         ["-","-","-"]]
player = "X"
for i in range(0,len(board)):
    for w in range(len(board)):
        print(board[i][w], end= "")
    print("")
while 1 == 1:   
    x = input("Input your x coordinate")
    y = input("Input your y coordinate")
    
    while(not x.isdigit() or not y.isdigit()):
        print(isinstance(x, int))
        print ("You can only enter 0, 1, or 2")
        x = input("Input your x coordinate")
        y = input("Input your y coordinate")
    else:
        x = int(x)
        y = int(y)
    
    while(x>2 or x<0 or y>2 or y<0):
        print ("You can only enter 0, 1, or 2. Out of Bounds")
        x = int(input("Input your x coordinate"))
        y = int(input("Input your y coordinate"))
    
    board[2-y][x] = player
    
    if (board[0][0]==player and board[1][1]==player and board[2][2]==player):
        print ("Player "+ player +" Wins!")
        exit()
    if (board[2][0]==player and board[1][1]==player and board[0][2]==player):
        print ("Player "+ player +" Wins!")
        exit()
    if (board[y][0]==player and board[y][1]==player and board[y][2]==player):
        print ("Player "+ player +" Wins!")
        exit()
        
    for i in range(0,len(board)):
        print(board[i], end= " ")
        print(" ")
    
    
    if (player == "X"):
        player = "O"
    else:
        player = "X"
    